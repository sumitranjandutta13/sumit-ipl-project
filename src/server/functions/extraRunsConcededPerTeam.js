/**
 * Takes matches arrayOfObject and deliveries arrayOfObject as parameters and creates a json file for extra runs conceded per team in 2016
 * @param {Array<{object}>} matches 
 * @param {Array<{object}>} deliveries
 * @return {Array<{objects}>}
 */
module.exports = function(matches, deliveries){

/**
 * matchId contains all the match_id of the year 2016 matches
 * @type {Array<string>}
 */
    const matchId = matches
        .filter((match) =>{
            if(match.season === '2016')
                return match.id;   
        })
        .map(match => match.id);
    
/**
 * extraRunsPerTeam contains all teams and extra runs conceded by them in 2016 
 * @type {{string: Number}}
 */
    const extraRunsPerTeam = deliveries
        .filter((delivery) => {
            if(matchId.includes(delivery.match_id)){
                return delivery;
            }
        })
        .reduce((accumulator, delivery) =>{
            
            let team = delivery.bowling_team;

            if(accumulator[team] === undefined){
                accumulator[team] = parseInt(delivery.extra_runs);
            }else{
                accumulator[team] += parseInt(delivery.extra_runs);
            }

            return accumulator;
        },{});
    
/**
 * bowlersRunsConceded contains all the bowlers name and their number of balls bowled and runs conceded
 * @type {Array<{team:string, extra_runs: Number}>}
 */
    const extraRunsPerTeamArray = [];
    Object.keys(extraRunsPerTeam)
        .forEach(key => extraRunsPerTeamArray.push({
            team : key,
            extra_runs : extraRunsPerTeam[key]
        }));
    
    return extraRunsPerTeamArray;
}