/**
 * Takes matches arrayOfObject and deliveries arrayOfObject as parameters and creates a json file for top 10 economical bowlers
 * @param {Array<{object}>} matches 
 * @param {Array<{object}>} deliveries
 * @return {Array<{objects}>}
 */
module.exports = function(matches, deliveries){

/**
 * matchId contains all the match_id of the year 2015 matches
 * @type {Array<string>}
 */
    const matchId = matches
        .filter((match) =>{
            if(match.season === '2015')
                return match.id;   
        })
        .map(match => match.id);

/**
 * bowlersRunsConceded contains all the bowlers name and their number of balls bowled and runs conceded
 * @type {{string: {runs:Number,balls: Number}}}
 */
    const bowlersRunsConceded = deliveries
        .filter((delivery) => {
            if(matchId.includes(delivery.match_id)){
                return delivery;
            }
        })
        .reduce((accumulator, delivery) => {

            let bowler = delivery.bowler;
            let runs = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) + parseInt(delivery.batsman_runs);
            
            if(delivery.is_super_over === '1')
                return accumulator;
            
            if(accumulator[bowler] === undefined && delivery.noball_runs !== '1'){
                accumulator[bowler] = {};
                accumulator[bowler]['runs'] = runs;
                accumulator[bowler]['balls'] = 1;
            }else if(accumulator[bowler] === undefined){
                accumulator[bowler] = {};
                accumulator[bowler]['runs'] = runs;
                accumulator[bowler]['balls'] = 0;
            }else{
                accumulator[bowler]['runs'] += runs;
                accumulator[bowler]['balls'] += 1;
            }
            return accumulator;
        }, {});

/**
 * runsConcededArray contains all the bowlers and their stats for the season
 * @type {Array<{name: string, stats: {balls: Number, runs: Number}}>}
 */
    const bowlersRunsConcededArray = [];
    Object.keys(bowlersRunsConceded)
        .forEach(key => bowlersRunsConcededArray.push({
            name : key, 
            stats : bowlersRunsConceded[key]
        }));
    
/**
 * runsConcededArray top 10 economical the bowlers and their stats for the season
 * @type {Array<{name: string, stats: {balls: Number, runs: Number, economy: Number}}>}
 */
    const mostEconomicalBowlers = bowlersRunsConcededArray
        .map(bowler => {
            let economy = parseFloat((bowler['stats']['runs']/ bowler['stats']['balls'] * 6).toFixed(2));
            bowler['stats']['economy'] = economy;
            return bowler;
        })
        .sort((a,b) => {
            return a.stats.economy - b.stats.economy;
        }).splice(0,10);

    return mostEconomicalBowlers;
}