/**
 * Takes matches arrayOfObject and create json file for matches per year
 * @param {Array<{object}>} matches
 * @return {Array<{objects}>}
 */
module.exports = function(matches){
    
/**
 * contains all years and number of matches played in that year
 * @type {{string : Number}}
 */
    const perYearMatches = matches.reduce((accumulator, element) => {
        let edition = element.season;
        if(accumulator[edition] === undefined){
            accumulator[edition] = 1;
        }else{
            accumulator[edition] += 1;
        }
        return accumulator;
    }, {});

/**
 * contains all years and number of matches played in that year
 * @type {Array<{year:string , matches: Number}>}
 */
    const perYearMatchesArray = [];
    Object.keys(perYearMatches)
        .forEach(key => perYearMatchesArray.push({
            year : key,
            matches : perYearMatches[key]
        }));

    return perYearMatchesArray;
}