const fs = require('fs');

/**
 * matchesPerYear function imported from the file matchesPerYear.js
 * @type {function}
 */
const matchesPerYear = require('./functions/matchesPerYear');

/**
 * matchesWonPerYear function imported from the file matchesPerWonYear.js
 * @type {function}
 */
const matchesWonPerYear = require('./functions/matchesWonPerYear');

/**
 * extraRunsConcededPerTeam imported from the file extraRunsConcededPerTeam.js
 * @type {function}
 */
const extraRunsConcededPerTeam = require('./functions/extraRunsConcededPerTeam');

/**
 * economicalBowlers function imported from the file economicalBowlers.js
 * @type {function}
 */
const economicalBowlers = require('./functions/economicalBowlers');

/**
 * matches data from matchesData.json file
 * @type {Array}
 */
let matchesData = require('../data/matchesData.json');

/**
 * deliveries data from deliveriesData.json file
 * @type {Array}
 */
let deliveriesData = require('../data/deliveriesData.json');

/**
 * number matches played every year
 * @type {string}
 */
const matchesPerYearData = JSON.stringify(matchesPerYear(matchesData));

/**
 * number of matches won by every team each year
 * @type {string}
 */
const matchesWonPerYearData = JSON.stringify(matchesWonPerYear(matchesData));

/**
 * extra runs conceded by every team in 2016
 * @type {string}
 */
const extraRunsConcededPerTeamData = JSON.stringify(extraRunsConcededPerTeam(matchesData, deliveriesData));

/**
 * top 10 economical bowlers of season 2015
 * @type {string}
 */
const economicalBowlersData = JSON.stringify(economicalBowlers(matchesData, deliveriesData));

fs.writeFile('src/public/output/matchesPerYear.json', matchesPerYearData, err => {
    console.log("Matches Played Per Year Data Created!!!!")
    if(err) throw err;
  }
);

fs.writeFile('src/public/output/matchesWonPerYear.json', matchesWonPerYearData, err => {
    console.log("Matches Won Per Year Data Created!!!!")
    if(err) throw err;
  }
);

fs.writeFile('src/public/output/extraRunsConcededPerTeam.json', extraRunsConcededPerTeamData, err => {
    console.log("Extra Runs Conceded Per Team in 2016 Data Created!!!!");
     if(err) throw err;
  }
);

fs.writeFile('src/public/output/top10EconomicalBowler2015.json', economicalBowlersData, err => {
    console.log("Top 10 Economical Bowlers in 2015 Data Created!!!!");
    if(err) throw err;
  }
);